<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー新規登録</title>
</head>
<body>
	<div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>

		<a href=management>ユーザー管理</a>
		<form action="signup" method = "post"><br />

			<label for="account">アカウント名</label>
			<p>アカウント名は半角英数字は6字以上20文字以下で入力してください</p>
			<input name="account" id="account" value="${user.account}"/> <br />

			<label for="password">パスワード</label>
			<p>パスワードは記号を含む半角文字で6文字以上20文字以下で入力してください</p>
			<input name="password" type="password"  id="password" /> <br />

			<label for="checkPassword">確認用パスワード</label>
			<input name="checkPassword" type="Password"  id="checkPassword" /> <br />

			<label for="name">名前</label>
			<p>名前は10文字以下で入力してください</p>
			<input name="name" id="name" value="${user.name}" />  <br>

			<label for="branchId">支社</label>
			<select name="branchId">
				<c:forEach items="${branches}" var="branch">
					<option value="${branch.id}" <c:if test="${user.branchId == branch.id}">selected</c:if>>
						<c:out value="${branch.name}"/>
					</option>
				</c:forEach>
			</select><br>

			<label for="departmentId">部署</label>
			<select name="departmentId">
				<c:forEach items="${departments}" var="department">
					<option value="${department.id}" <c:if test="${user.departmentId == department.id}">selected</c:if>>
						<c:out value="${department.name}"/>
					</option>
				</c:forEach>
			</select><br>

			<input type="submit" value="登録"><br />
		</form>

			<div class="copyright">Copyright(c) suzuki.satoru</div>
	</div>

</body>
</html>