<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー新規登録</title>

<script type="text/javascript">
		//停止ボタンダイアログ
		function stop() {
			if(window.confirm('アカウントを停止しますか？')) {
				return true;
			} else {
				window.alert('キャンセルされました');
				return false;
			}
		}
		//復活ボタンダイアログ
		function restart() {
			if(window.confirm('アカウントを復活させますか？')) {
				return true;
			} else {
				window.alert('キャンセルされました');
				return false;
			}
		}
</script>
</head>
<body>
	<div>
		<c:if test="${ not empty errorMessages }">
			<div class = "errorMessages">
				<ul>
					<c:forEach items = "${errorMessages}" var = "errorMessage">
						<li><c:out value = "${errorMessage}" />
					</c:forEach>
				</ul>
				<c:remove var="errorMessages" scope="session" />
			</div>
		</c:if>

		<a href="./">ホーム</a>
		<a href="signup">ユーザー新規登録</a><br>

		<c:forEach items="${userBranchDepartments}" var="userBranchDepartment">
			<label for="account">アカウント</label>
			<c:out value="${userBranchDepartment.account}" /><br>

			<label for="name">名前</label>
			<c:out value="${userBranchDepartment.name}" /><br>

			<label for="branchId">支社</label>
			<c:out value="${userBranchDepartment.branch}" /><br>

			<label for ="departmentId">部署</label>
			<c:out value="${userBranchDepartment.department}" /><br>


			<label for="accountStatus">アカウント復活停止状態</label><br>
			<c:out value="${userBranchDepartment.isStopped}" /><br>

			<form action="setting" method="get">
				<input type="hidden" value="${userBranchDepartment.id}" name="settingUserId">
				<input type="submit" value="編集">
			</form>

			<c:if test="${loginUser.id != userBranchDepartment.id}">
				<c:if test="${userBranchDepartment.isStopped == 0}">
					<form action="stop" method="post" onsubmit="return stop()">
						<input type="submit" value="停止">
						<input type="hidden" value="${userBranchDepartment.id}" name="statusId">
						<input type="hidden" value="1" name="isStopped">
					</form>
				</c:if>

				<c:if test="${userBranchDepartment.isStopped == 1}">
					<form action="stop" method="post" onsubmit="return restart()">
						<input type="submit" value="復活">
						<input type="hidden" value="${userBranchDepartment.id}" name="statusId">
						<input type="hidden" value="0" name="isStopped">
					</form>
				</c:if>
			</c:if>
		</c:forEach>

			<div class="copyright">Copyright(c) suzuki.satoru</div>
	</div>

</body>
</html>