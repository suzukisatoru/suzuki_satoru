<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ホーム画面</title>

	<script type="text/javascript">
		function checkDelete(){
			if(window.confirm("削除しますか？")){
				return true;
			}else{
				window.alert("削除をキャンセルしました");
				return false
			}
		}
	</script>

</head>
<body>
	<div class="home-contents">


		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
					<c:remove var="errorMessages" scope="session" />
				</ul>
			</div>
		</c:if>

		<form action="./" method="get">
			<label>日付</label>
			<input type="date" name="startDate" value="${startDate}">~
			<input type="date" name="endDate" value="${endDate}"><br>
			<label>カテゴリ</label>
			<input type="text" name="category" value="${category}"><br>
			<input type="submit" value="絞り込み">
		</form>

		<div class="header">
				<a href="message">新規投稿</a>
				<c:if test="${loginUser.branchId == 1  && loginUser.departmentId == 1 }">
					<a href="management">ユーザー管理</a>
				</c:if>
				<a href="logout">ログアウト</a>
		</div>
		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
						<div class="account">ユーザー名: <c:out value="${message.account}" /></div><br>
						<div class="title">件名: <c:out value="${message.title}" /></div><br>
						<div class="category">カテゴリー: <c:out value="${message.category}" /></div><br>
						<div class="text">本文<br>
							<c:forEach var="text" items="${fn:split(message.text, '
							') }">
									<c:out value="${text}" /><br>
							</c:forEach>
						</div>
						<div class="date">
							投稿日時
							<fmt:formatDate value="${message.createdDate}"
								pattern="yyyy/MM/dd HH:mm:ss" /><br>
						</div>
						<c:if test="${message.userId == loginUser.id}">
							<form action="deleteMessage" method="post" onSubmit="return checkDelete()">
								<input name ="deleteMessageId" value="${message.id}" type="hidden">
								<input type="submit" value="削除">
							</form>
						</c:if>
				</div>

				<c:forEach items="${comments}" var="comment">
					<c:if test="${comment.messageId == message.id }">
						<div class="commentAccount">ユーザー名: <c:out value="${comment.account}"/><br></div>
						<div class="commentText">コメント内容<br>
							<c:forEach var="commentText" items="${fn:split(comment.text, '
							') }">
									<c:out value="${commentText}"/><br>
							</c:forEach>
						</div>
						<div class="commentDate">コメント日時: <c:out value="${comment.createdDate}"/><br></div>

						<c:if test="${comment.userId == loginUser.id}">
							<form action="deleteComment" method="post" onSubmit="return checkDelete()">
								<input name ="deleteCommentId" value="${comment.id}" type="hidden">
								<input type="submit" value="コメント削除"><br>
							</form>
						</c:if>
					</c:if>
				</c:forEach>

				<div class="comments">
					<form action="comment" method="post">

						<label>コメント入力欄</label><br>
						<textarea name="text"></textarea><br>
						<input name="messageId" type ="hidden" value="${message.id}" >
						<input type="submit" value="コメント投稿">
					</form>
				</div>

			</c:forEach>

		</div>

		<div class="copyright">Copyright(c)suzuki.satoru</div>
	</div>
</body>
</html>