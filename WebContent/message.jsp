<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規投稿</title>
</head>
<body>
	<div>
		<c:if test="${ not empty errorMessages }">
			<div class = "errorMessages">
				<ul>
					<c:forEach items = "${errorMessages}" var = "errorMessage">
						<li><c:out value = "${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>
		<a href="./">ホーム</a>
		<form action="message" method="post" ><br />

			<label for="title" >件名</label>
			<input name="title" id="title" value="${title}"/> <br />

			<label for="catecory">カテゴリ</label>
			<input name="category" id="category" value="${category}" /> <br />

			<label for="text">投稿内容</label>
			<textarea name="text" id="text">${text}</textarea><br />

			<input type="submit" value="投稿">
		</form>

		<div class="copyright">Copyright(c) suzuki.satoru</div>
	</div>

</body>
</html>