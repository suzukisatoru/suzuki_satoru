<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー編集</title>
</head>
<body>

	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>

		<form action="setting" method="post"><br />
			<input name="id" value="${user.id}" id="id" type="hidden" />

			<label for="name">アカウント名</label>
			<p>アカウント名は半角英数字は6字以上20文字以下で入力してください</p>
			<input name="account" value="${user.account}" id="account"/><br />

			<label for="password">パスワード</label>
			<p>パスワードは記号を含む半角文字で6文字以上20文字以下で入力してください</p>
			<input name="password" type="password"id="password" /><br />

			<label for="checkPassword">確認用パスワード</label>
			<input name="checkPassword" type="password" id="checkPassword"/><br>

			<label for="name">名前</label>
			<p>名前は10文字以下で入力してください</p>
			<input name="name" value="${user.name}" id="name"/> <br />

			<label for ="branchId">支社</label>
			<select name="branchId">
				<c:forEach items="${branches}" var="branch">
					<c:choose>
						<c:when test="${loginUser.id != user.id}">
							<option value="${branch.id}" <c:if test="${user.branchId == branch.id}">selected</c:if>>
								<c:out value="${branch.name}"/>
							</option>
						</c:when>

						<c:otherwise>
							<option value="${branch.id}" <c:if test="${user.branchId != branch.id}">disabled</c:if>>
								<c:out value="${branch.name}"/>
							</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select><br>

			<label for ="departmentId">部署</label>
			<select name="departmentId">
				<c:forEach items="${departments}" var="department">
					<c:choose>
						<c:when test="${loginUser.id != user.id}">
							<option value="${department.id}" <c:if test="${user.departmentId == department.id}">selected</c:if>>
								<c:out value="${department.name}"></c:out>
							</option>
						</c:when>
						<c:otherwise>
							<option value="${department.id}" <c:if test="${user.departmentId != department.id}">disabled</c:if>>
								<c:out value="${department.name}"></c:out>
							</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select><br>
			<input type = "submit" value = "更新"><br />
		</form>

		<div class="copyright">Copyright(c)suzukisatoru</div>
	</div>

</body>
</html>