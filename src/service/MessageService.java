package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//メッセージ一覧を取得する
	public List<UserMessage> select(String startDate, String endDate, String category) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		//startDateが入力されている場合
		if (!StringUtils.isBlank(startDate)) {
			startDate += " 00:00:00";
		//入力されていない場合のデフォルト値
		} else {
			startDate = "2021-01-01 00:00:00";

		}
		//endDateが入力されている場合
		if (!StringUtils.isBlank(endDate)) {
			endDate += " 23:59:59";
		//入力されていない場合のデフォルト値
		} else {
			endDate = String.valueOf(sdf.format(new Date()));

		}
		//カテゴリが入力されている場合
		if (!StringUtils.isBlank(category)) {
			category = "%" + category + "%";
		//入力されていない場合のデフォルト値
		} else {
			category = "%_%";
		}

		Connection connection = null;
		try {
			connection = getConnection();
			List<UserMessage> messages = new UserMessageDao().select(connection, startDate, endDate, category);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//メッセージの削除
	public void delete(Message deleteMessage) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, deleteMessage);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}