package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		//loginUserの情報を取得
		User loginUser = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<>();

		//loginUserに情報が入っているか、アクセスするページがログインのURLだった場合
		if (loginUser != null || ((HttpServletRequest) request).getServletPath().equals("/login")) {
			chain.doFilter(request, response); //サーブレットを実行
		} else {
			errorMessages.add("ログインしてください");
			//エラーメッセージをセットし、ログインページにリダイレクトする
			((HttpServletRequest) request).getSession().setAttribute("errorMessages", errorMessages);
			((HttpServletResponse) response).sendRedirect("login");
		}

	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}
