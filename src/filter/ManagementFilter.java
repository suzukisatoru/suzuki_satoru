package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;

@WebFilter(urlPatterns = { "/setting", "/signup", "/management" })
public class ManagementFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		//loginUserの情報を取得
		User loginUser = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<>();

		//loginUserの支社と部署のIDを取得
		int userBranchId = loginUser.getBranchId();
		int userDepartmentId = loginUser.getDepartmentId();

		if (userBranchId == 1 && userDepartmentId == 1) {
			chain.doFilter(request, response);
		} else {
			errorMessages.add("権限がありません");

			((HttpServletRequest) request).getSession().setAttribute("errorMessages", errorMessages);
			((HttpServletResponse) response).sendRedirect("./");

		}

	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}
