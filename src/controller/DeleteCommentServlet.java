package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import service.CommentService;

@WebServlet(urlPatterns = { "/deleteComment" })
public class DeleteCommentServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

		//削除するコメントのidを取得
		int deleteCommentId = Integer.parseInt(request.getParameter("deleteCommentId"));
		//System.out.println(deleteMessageId);

		Comment deleteComment = new Comment();
		deleteComment.setId(deleteCommentId);

		new CommentService().delete(deleteComment);

		response.sendRedirect("./");
    }

}