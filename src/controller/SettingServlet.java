package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//編集するユーザーIDを取得 matches使うためStringに変更
		String settingUserId = request.getParameter("settingUserId");
		List<String> errorMessages = new ArrayList<>();
		HttpSession session = request.getSession();

		//不正なパラメータ
		if (StringUtils.isEmpty(settingUserId) || !settingUserId.matches("^[0-9]*$")) {
			errorMessages.add("不正なパラメータが入力されました");
			//sessionに格納し、リダイレクト
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("management");
			return;
		}

		User user = new UserService().select(Integer.parseInt(settingUserId));

		//不正なパラメータ(userを作成してnull比較)
		if (user == null) {
			errorMessages.add("不正なパラメータが入力されました");
			//sessionに格納し、リダイレクト
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("management");
			return;
		}

		System.out.println(errorMessages);

		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();

		request.setAttribute("user", user);
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<String> errorMessages = new ArrayList<String>();
		User user = getUser(request);

		String checkPassword = request.getParameter("checkPassword");
		if (!isValid(user, checkPassword, errorMessages)) {
			List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();

			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("user", user);

			request.getRequestDispatcher("setting.jsp").forward(request, response);

			return;
		}

		new UserService().update(user);
		response.sendRedirect("management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

		return user;
	}

	private boolean isValid(User user, String checkPassword, List<String> errorMessages) {

		String account = user.getAccount();
		String password = user.getPassword();
		String name = user.getName();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		//アカウント名重複用に取得
		User diffUser = new UserService().select(account);

		//アカウント
		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");
		} else if (20 <= account.length() || 6 >= account.length()) {
			errorMessages.add("アカウント名は6文字以上20文字以下で入力してください");
		} else if (account.matches("/^[0-9a-zA-Z]*$/")) {
			errorMessages.add("アカウント名は半角英数字で入力してください");
		} else if (diffUser != null && diffUser.getId() != user.getId()) {
			errorMessages.add("アカウント名が重複しています");
		}

		//パスワード
		if (!password.equals(checkPassword)) {
			errorMessages.add("確認用パスワードと一致しません");
			//何かしら入力されているとき
		} else if (!StringUtils.isEmpty(password) && (20 <= password.length() || 6 >= password.length())) {
			errorMessages.add("パスワードは6文字以上20文字以下で入力してください");
		} else if (!StringUtils.isEmpty(password) && password.matches("[ -~｡-ﾟ]")) {
			errorMessages.add("パスワードは半角文字で入力してください");
		}

		//名前
		if (StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください");
		} else if (name.length() >= 10) {
			errorMessages.add("名前は10文字以内で入力してください");
		}

		//支社と部署
		if (branchId == 1 && departmentId >= 3) {
			errorMessages.add("本社と部署の選択が一致しません");
		} else if (branchId >= 2 && departmentId <= 2) {
			errorMessages.add("支社と部署の選択が一致しません");
		}

		if (errorMessages.size() != 0) {
			return false;
		}

		return true;
	}
}