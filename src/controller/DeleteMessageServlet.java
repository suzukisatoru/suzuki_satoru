package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Message;
import service.MessageService;

@WebServlet(urlPatterns = { "/deleteMessage" })
public class DeleteMessageServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//削除するメッセージのidを取得
		int deleteMessageId = Integer.parseInt(request.getParameter("deleteMessageId"));
		//System.out.println(deleteMessageId);

		Message deleteMessage = new Message();
		deleteMessage.setId(deleteMessageId);

		new MessageService().delete(deleteMessage);

		response.sendRedirect("./");
	}

}