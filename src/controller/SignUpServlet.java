package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();

		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

		User user = getUser(request);

		if (!isValid(user, errorMessages)) {
			List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();

			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("user", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}

		new UserService().insert(user);
		response.sendRedirect("management");
	}

	//入力された値を取得
	private User getUser(HttpServletRequest request) throws IOException, ServletException {
		User user = new User();
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		//確認用パスワード
		user.setCheckPassword(request.getParameter("checkPassword"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
		return user;
	}

	//新規登録のバリデーション
	private boolean isValid(User user, List<String> errorMessages) {

		String account = user.getAccount();
		String password = user.getPassword();
		String checkPassword = user.getCheckPassword();
		String name = user.getName();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		//アカウント
		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");
		} else if (20 <= account.length() || 6 >= account.length()) {
			errorMessages.add("アカウント名は6文字以上20文字以下で入力してください");
		} else if (account.matches("/^[0-9a-zA-Z]*$/")) {
			errorMessages.add("アカウント名は半角英数字で入力してください");
		} else if (new UserService().select(account) != null) {
			errorMessages.add("アカウント名が重複しています");
		}

		//パスワード
		if (StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力してください");
		} else if (20 <= password.length() || 6 >= password.length()) {
			errorMessages.add("パスワードは6文字以上20文字以下で入力してください");
		} else if (password.matches("[ -~｡-ﾟ]")) {
			errorMessages.add("パスワードは半角文字で入力してください");
		} else if (!password.equals(checkPassword)) {
			errorMessages.add("確認用パスワードと一致しません");
		}

		//名前

		if (StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください");
		} else if (!StringUtils.isBlank(name) && (10 <= name.length())) {
			errorMessages.add("名前は10文字以下で入力してください");
		}

		//支社と部署
		if (branchId == 1 && departmentId >= 3) {
			errorMessages.add("本社と部署の選択が一致しません");
		} else if (branchId >= 2 && departmentId <= 2) {
			errorMessages.add("支社と部署の選択が一致しません");
		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;

	}

}
