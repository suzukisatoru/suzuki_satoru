package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//復活停止ボタンの値を取得
		User userStatus = new User();
		userStatus.setId(Integer.parseInt(request.getParameter("statusId")));
		userStatus.setIsStopped(Integer.valueOf(request.getParameter("isStopped")));

		new UserService().updateStatus(userStatus);

		response.sendRedirect("management");

	}

}
