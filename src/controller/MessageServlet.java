package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("message.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

		Message message = getMessage(request);
		if (!isValid(message, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			//入力失敗した時の値を保持
			request.setAttribute("title", message.getTitle());
			request.setAttribute("category", message.getCategory());
			request.setAttribute("text", message.getText());
			request.getRequestDispatcher("message.jsp").forward(request, response);
			return;
		}

		new MessageService().insert(message);
		response.sendRedirect("./");
	}

	//入力された値を取得
	public Message getMessage(HttpServletRequest request) {

		HttpSession session = request.getSession();

		Message message = new Message();
		message.setTitle(request.getParameter("title"));
		message.setCategory(request.getParameter("category"));
		message.setText(request.getParameter("text"));

		User user = (User) session.getAttribute("loginUser");
		message.setUserId(user.getId());

		return message;
	}

	private boolean isValid(Message message, List<String> errorMessages) {

		String title = message.getTitle();
		String category = message.getCategory();
		String text = message.getText();

		//空白スペース対策でisBlankに変更
		//件名(title)
		if (StringUtils.isBlank(title)) {
			errorMessages.add("件名を入力してください");
		} else if (30 <= title.length()) {
			errorMessages.add("件名は30文字以下で入力してください");
		}

		//カテゴリ(category)
		if (StringUtils.isBlank(category)) {
			errorMessages.add("カテゴリを入力してください");
		} else if (10 <= category.length()) {
			errorMessages.add("カテゴリは10文字以下で入力してください");
		}
		//本文(text)
		if (StringUtils.isBlank(text)) {
			errorMessages.add("本文を入力してください");
		} else if (1000 <= text.length()) {
			errorMessages.add("本文は1000文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

}