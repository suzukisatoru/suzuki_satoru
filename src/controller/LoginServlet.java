package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		request.getRequestDispatcher("login.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<>();
		HttpSession session = request.getSession();

		String account = request.getParameter("account");
		String password = request.getParameter("password");
		User user = null;

		if (isValid(account, password, errorMessages)) {

			user = new UserService().select(account, password);
			//取得したuserにnullが返ってきた場合&停止状態だった時
			if (user == null || user.getIsStopped() == 1) {
				errorMessages.add("ログインに失敗しました");
			}

		}

		if (errorMessages.size() != 0) {
			request.setAttribute("errorMessages", errorMessages);
			//入力したアカウント名を保持
			request.setAttribute("account", account);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		session.setAttribute("loginUser", user);
		response.sendRedirect("./");
	}

	public boolean isValid(String account, String password, List<String> errorMessages) {

		//アカウント名入力無しエラー
		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名が入力されていません");
		}
		//パスワード入力無しエラー
		if (StringUtils.isBlank(password)) {
			errorMessages.add("パスワードが入力されていません");
		}
		if (errorMessages.size() != 0) {
			return false;
		}

		return true;
	}

}
